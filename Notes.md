# tools


## command line tool to manage repo
glc, pay attention to the .glc.yml files


export PROJECT_ID=20666366 
glc list project-jobs $PROJECT_ID | sed '1d' | jq '.[] | .id' | head -n1 # get latest job
glc get project-job-trace $PROJECT_ID $(glc list project-jobs $PROJECT_ID | sed '1d' | jq '.[] | .id' | head -n1)

## webdriver - to access the webpages
ssh -NfL 4444:localhost:4444 zhoupengyu@172.26.253.203

# Steps
- we just use the full interactive process to get ticket information.

- use the previous one as reference for quick review.

- start after logged in, we just use the cookie, we can stop at the order page. we just pay it manually.
